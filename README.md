# MkDocs Review Apps Example
This is an example `mkdocs` application that uses GitLab Review and Visual Review Apps.

## Build

Install the requirements for the build

```python
pip install -r requirements.txt
```

Build the docs

```bash
mkdocs build
```

## Dev

After installing the requirements above, run the following to serve the docs

```bash
mkdocs serve
```

You can the access the docs to http://localhost:8000/

